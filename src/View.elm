module View exposing (view)

import App exposing (..)
import Color exposing (black, darkYellow, grey, lightYellow, white, yellow)
import Dict
import Engine.Action exposing (..)
import Engine.Bug exposing (..)
import Engine.Coord exposing (..)
import Engine.Field exposing (..)
import Engine.Game exposing (..)
import Html exposing (..)
import TypedSvg exposing (g, polygon, rect, svg, text_)
import TypedSvg.Attributes exposing (fill, fontFamily, fontWeight, points, stroke, style, textAnchor, transform, viewBox)
import TypedSvg.Attributes.InPx exposing (fontSize, height, strokeWidth, width, x, y)
import TypedSvg.Events exposing (onClick)
import TypedSvg.Types exposing (AnchorAlignment(..), Fill(..), FontWeight(..), Transform(..))


coordToPix : ( Int, Int ) -> ( Float, Float )
coordToPix ( q, r ) =
    ( sqrt 3 * toFloat q + sqrt 3 / toFloat 2 * toFloat r
    , 3 / 2 * toFloat r
    )


theColor : Color -> Bool -> Color.Color
theColor color selected =
    case ( color, selected ) of
        ( Black, False ) ->
            black

        ( Black, True ) ->
            darkYellow

        ( White, False ) ->
            white

        ( White, True ) ->
            lightYellow


theInverseColor : Color -> Color.Color
theInverseColor color =
    case color of
        Black ->
            white

        White ->
            black


hex : List (Attribute msg) -> Html msg
hex attrs =
    polygon (attrs ++ [ points (adjacents ( 0, 0 ) |> List.map coordToPix |> List.map (\( x, y ) -> ( y / 2, x / 2 ))) ]) []


viewPiece : Color -> Bool -> List (Attribute msg) -> Html msg
viewPiece color isSelected attrs =
    hex
        (attrs
            ++ [ fill (Fill (theColor color isSelected))
               , stroke Color.lightCharcoal
               , strokeWidth 0.1
               ]
        )


viewBug : Bug -> Maybe Bug -> Html msg
viewBug bug selection =
    let
        ( color, species, _ ) =
            bug
    in
    g
        [ style "cursor: pointer" ]
        [ viewPiece color (selection == Just bug) []
        , text_ [ y 0.35, fontSize 1, fill (Fill (theInverseColor color)), textAnchor AnchorMiddle, fontWeight FontWeightBold ] [ text (speciesToString species) ]
        ]


viewField : Field -> Maybe Bug -> List Coord -> Html Msg
viewField field selection moves =
    g []
        ((Dict.toList field ++ List.map (\m -> ( m, [] )) moves)
            |> List.sortBy (\( ( q_, r_ ), _ ) -> ( r_, -q_ ))
            |> List.map
                (\( coord, bugs ) ->
                    let
                        ( x_, y_ ) =
                            coordToPix coord
                    in
                    g [ transform [ Translate x_ y_ ] ]
                        ((bugs
                            |> List.indexedMap
                                (\i bug ->
                                    g
                                        (transform [ Translate (toFloat i * 0.2) (toFloat i * -0.2) ]
                                            :: (if not (List.member coord moves) then
                                                    [ onClick (Select (Just bug)) ]

                                                else
                                                    []
                                               )
                                        )
                                        [ viewBug bug selection ]
                                )
                         )
                            ++ (if List.member coord moves then
                                    [ g
                                        [ style "cursor: pointer"
                                        , transform [ Translate (toFloat (List.length bugs) * 0.2) (toFloat (List.length bugs) * -0.2) ]
                                        , onClick (App.Move coord)
                                        ]
                                        [ hex [ stroke yellow, strokeWidth 0.2, fill (Fill black), style "opacity: 0" ]
                                        , hex [ stroke yellow, strokeWidth 0.2, fill FillNone ]
                                        ]
                                    ]

                                else
                                    []
                               )
                        )
                )
        )


viewReserve : List (List Bug) -> Maybe Bug -> Html Msg
viewReserve stacks selection =
    g []
        (stacks
            |> List.indexedMap
                (\c bugs ->
                    g [ transform [ Translate 0.0 (toFloat c * 2) ] ]
                        (bugs
                            |> List.indexedMap
                                (\i bug ->
                                    g
                                        (transform [ Translate (0.2 * toFloat i) (-0.2 * toFloat i) ]
                                            :: (if i == List.length bugs - 1 then
                                                    [ onClick (Select (Just bug)) ]

                                                else
                                                    []
                                               )
                                        )
                                        [ viewBug bug selection ]
                                )
                        )
                )
        )


viewPhase : Game -> Field -> Html Msg
viewPhase game field =
    case checkWin field of
        Just (Win color) ->
            g []
                [ viewPiece color False []
                , g [ transform [ Translate 0 0.2 ] ]
                    [ text_ [ fill (Fill (theInverseColor color)), textAnchor AnchorMiddle, fontSize 0.5, fontWeight FontWeightBold ] [ text "WIN" ] ]
                ]

        Just Draw ->
            g [] [ text_ [ textAnchor AnchorMiddle, fontSize 0.5, fontWeight FontWeightBold ] [ text "DRAW" ] ]

        Nothing ->
            let
                mustPass =
                    checkAction Nothing game
            in
            g
                (if mustPass then
                    [ onClick Pass ]

                 else
                    []
                )
                (viewPiece (whoseTurn game.actions) False []
                    :: (if mustPass then
                            [ g [ style "cursor: pointer", transform [ Translate 0 0.2 ] ]
                                [ text_ [ fill (Fill (theInverseColor (whoseTurn game.actions))), textAnchor AnchorMiddle, fontSize 0.5, fontWeight FontWeightBold ] [ text "PASS" ] ]
                            ]

                        else
                            []
                       )
                )


view : Model -> Html Msg
view model =
    let
        ( ( left_, top_ ), ( width_, height_ ) ) =
            ( ( -14, -14 ), ( 28, 28 ) )

        moves =
            model.selection
                |> Maybe.map (\bug -> genDestinations bug model.game)
                |> Maybe.withDefault []
    in
    div []
        [ svg
            [ viewBox left_ top_ width_ height_
            , width 560
            , TypedSvg.Attributes.InPx.height 560
            , fontFamily [ "sans-serif" ]
            ]
            [ rect [ x top_, y left_, width width_, TypedSvg.Attributes.InPx.height height_, fill (Fill grey) ] []
            , viewField model.field model.selection moves
            , g [ transform [ Translate (left_ + 1) (top_ + 1) ] ] [ viewReserve (reserve White model.game) model.selection ]
            , g [ transform [ Translate (left_ + width_ - 1) (top_ + 1) ] ] [ viewReserve (reserve Black model.game) model.selection ]
            , g [ transform [ Translate 0 (top_ + 1) ] ] [ viewPhase model.game model.field ]
            ]
        , div [] [ text (gameToString model.game) ]
        ]
