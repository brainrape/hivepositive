module Engine.Movement exposing (genMoves, genThrows)

import Engine.Bug exposing (..)
import Engine.Coord exposing (..)
import Engine.Field exposing (..)
import Maybe.Extra


type alias Movement =
    Coord -> Field -> List Coord


{-| ( head, field, trace )
-}
type alias Path =
    ( Coord, Field, List Coord )


head : Path -> Coord
head ( c, _, _ ) =
    c


checkCrawl : Coord -> Coord -> Field -> Bool
checkCrawl src dst field =
    checkGate src dst field && checkContact src dst field


checkGate : Coord -> Coord -> Field -> Bool
checkGate src dst field =
    genLR src dst |> List.any (\c -> height c field < max (height src field) (height dst field + 1))


checkContact : Coord -> Coord -> Field -> Bool
checkContact src dst field =
    ((genLR src dst |> List.map (\c -> height c field)) ++ [ height src field - 1, height dst field ]) |> List.any (\h -> h > 0)


genCrawls : Movement
genCrawls src field =
    src |> adjacents |> List.filter (\dst -> checkCrawl src dst field && (height dst field == height src field - 1))


genClimbs : Movement
genClimbs src field =
    adjacents src
        |> List.filter (\dst -> height dst field >= height src field && checkCrawl src dst field)


genFalls : Movement
genFalls src field =
    adjacents src
        |> List.filter (\dst -> height dst field < height src field - 1 && checkCrawl src dst field)


genGrasshopperMoves : Movement
genGrasshopperMoves src field =
    let
        rec dir oldDst =
            let
                dst =
                    add oldDst dir
            in
            if isEmpty dst field then
                [ dst ]

            else
                rec dir dst
    in
    adjacents ( 0, 0 )
        |> List.filter (\dir -> not (isEmpty (add dir src) field))
        |> List.concatMap (\dir -> rec dir (add dir src))


steps : Movement -> Path -> List Path
steps gen ( src, field, visited ) =
    gen src field
        |> List.filter (\c -> not (List.member c visited))
        |> List.map (\c -> ( c, moveBug src c field, c :: visited ))


concatSteps : Movement -> List Path -> List Path
concatSteps gen paths =
    paths |> List.concatMap (steps gen)


genAntMoves : Movement
genAntMoves src field =
    let
        iterate xs =
            case xs of
                [] ->
                    []

                _ ->
                    xs ++ iterate (concatSteps genCrawls xs)
    in
    [ ( src, field, [ src ] ) ]
        |> iterate
        |> List.map head
        |> List.filter ((/=) src)


genSpiderMoves : Movement
genSpiderMoves src field =
    [ ( src, field, [ src ] ) ]
        |> concatSteps genCrawls
        |> concatSteps genCrawls
        |> concatSteps genCrawls
        |> List.map head


genBeetleMoves : Movement
genBeetleMoves src field =
    []
        ++ genClimbs src field
        ++ genCrawls src field
        ++ genFalls src field


genLadybugMoves : Movement
genLadybugMoves src field =
    [ ( src, field, [ src ] ) ]
        |> concatSteps genClimbs
        |> concatSteps genBeetleMoves
        |> List.filter (\( c, _, _ ) -> height c field > 0)
        |> concatSteps genFalls
        |> List.filter (\( c, _, _ ) -> isEmpty c field)
        |> List.map head


genMosquitoMoves : Movement
genMosquitoMoves src field =
    if height src field == 1 then
        adjacents src
            |> List.filterMap (\c -> getTopBug c field)
            |> List.map (\( _, s, _ ) -> s)
            |> List.filter (not << (==) Mosquito)
            |> List.concatMap (\s -> genMoves s src field)

    else
        genBeetleMoves src field


genThrows : Color -> Maybe Bug -> Movement
genThrows player resting src field =
    -- generate possible ways in which player's pillbugs or pillbug-stealing mosquitos can move this bug
    let
        filterTopBugs p coords =
            coords
                |> List.filterMap (\c -> getTopBug c field |> Maybe.Extra.filter p |> Maybe.andThen (always (Just c)))

        adjacentNonResting =
            adjacents src
                |> filterTopBugs (\bug -> Just bug /= resting)

        adjacentPillbugs =
            adjacentNonResting
                |> filterTopBugs (\( color, species, _ ) -> color == player && species == Pillbug)

        adjacentMosquitosCloningPillbugs =
            adjacentNonResting
                |> filterTopBugs (\( color, species, _ ) -> color == player && species == Mosquito)
                |> List.filter
                    (\mosquitoCoord ->
                        adjacents mosquitoCoord
                            |> filterTopBugs (\( _, species, _ ) -> species == Pillbug)
                            |> (not << List.isEmpty)
                    )

        adjacentThrowers =
            adjacentPillbugs ++ adjacentMosquitosCloningPillbugs
    in
    [ ( src, field, [ src ] ) ]
        |> concatSteps genClimbs
        |> List.filter (\( c, _, _ ) -> List.member c adjacentThrowers)
        |> concatSteps genFalls
        |> List.filter (\( c, _, _ ) -> isEmpty c field)
        |> List.map head


genMoves : Species -> Movement
genMoves species =
    case species of
        QueenBee ->
            genCrawls

        Grasshopper ->
            genGrasshopperMoves

        Spider ->
            genSpiderMoves

        Ant ->
            genAntMoves

        Beetle ->
            genBeetleMoves

        Ladybug ->
            genLadybugMoves

        Mosquito ->
            genMosquitoMoves

        Pillbug ->
            genCrawls
