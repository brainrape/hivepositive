module Engine.Field exposing (..)

import Dict exposing (..)
import Engine.Bug exposing (..)
import Engine.Coord exposing (..)


type alias Field =
    Dict Coord (List Bug)


stackToString : ( Coord, List Bug ) -> String
stackToString ( coord, bugs ) =
    coordToString coord ++ " " ++ String.join "; " (List.map bugToString bugs)


fieldToString : Field -> String
fieldToString field =
    field |> Dict.toList |> List.map stackToString |> String.join ","


get : Coord -> Field -> List Bug
get coord field =
    Dict.get coord field |> Maybe.withDefault []


getTopBug : Coord -> Field -> Maybe Bug
getTopBug coord field =
    get coord field |> List.reverse |> List.head


isEmpty : Coord -> Field -> Bool
isEmpty coord field =
    get coord field == []


height : Coord -> Field -> Int
height coord field =
    field |> get coord |> List.length


getCoord : Bug -> Field -> Maybe Coord
getCoord bug field =
    field |> Dict.filter (\_ stack -> List.member bug stack) |> Dict.keys |> List.head


addBug : Coord -> Bug -> Field -> Field
addBug coord bug field =
    field |> Dict.insert coord (get coord field ++ [ bug ])


removeBug : Bug -> Field -> Field
removeBug bug field =
    field
        |> Dict.map (\_ -> List.filter ((/=) bug))
        |> Dict.filter (\_ -> (/=) [])


moveBug : Coord -> Coord -> Field -> Field
moveBug src dst field =
    getTopBug src field
        |> Maybe.map (\bug -> field |> removeBug bug |> addBug dst bug)
        |> Maybe.withDefault field


isSurrounded : Coord -> Field -> Bool
isSurrounded coord field =
    adjacents coord
        |> List.filter (\c -> height c field > 0)
        |> List.length
        |> (==) 6


oneHive : Field -> Bool
oneHive field =
    let
        coords =
            field |> Dict.keys

        visit x visited unvisited =
            adjacents x |> List.filter (\c -> List.member c coords && not (List.member c visited) && not (List.member c unvisited))

        rec visited unvisited =
            case unvisited of
                x :: xs ->
                    rec (x :: visited) (xs ++ visit x visited unvisited)

                [] ->
                    visited
    in
    coords
        |> List.head
        |> Maybe.map (\root -> List.length (rec [] [ root ]) == List.length coords)
        |> Maybe.withDefault True
