module Engine.Action exposing (..)

import Dict
import Engine.Bug exposing (..)
import Engine.Coord exposing (..)
import Engine.Field exposing (..)
import List.Extra
import Maybe.Extra


type alias Action =
    Maybe ( Bug, Coord )


actionToString : Action -> String
actionToString act =
    case act of
        Just ( bug, coord ) ->
            bugToString bug ++ coordToString coord

        Nothing ->
            "pass"


checkPlace : Bug -> Coord -> Field -> Bool
checkPlace ( color, _, _ ) dst field =
    let
        adjacentTopBugs =
            adjacents dst |> List.filterMap (\coord -> getTopBug coord field)
    in
    True
        && isEmpty dst field
        && (adjacentTopBugs |> (not << List.any (\( c, _, _ ) -> color /= c)))
        && (adjacentTopBugs |> List.any (\( c, _, _ ) -> color == c))


genPlaces : Bug -> Int -> Field -> List Coord
genPlaces bug turnNumber field =
    case turnNumber of
        0 ->
            if bug |> (\( c, s, _ ) -> c == White && s /= QueenBee) then
                [ ( 0, 0 ) ]

            else
                []

        1 ->
            if bug |> (\( c, s, _ ) -> c == Black && s /= QueenBee) then
                adjacents ( 0, 0 )

            else
                []

        _ ->
            field
                |> Dict.toList
                |> List.concatMap (\( c, _ ) -> adjacents c)
                |> List.Extra.unique
                |> List.filter (\dst -> checkPlace bug dst field)
