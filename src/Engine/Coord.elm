module Engine.Coord exposing (..)


type alias Coord =
    -- Axial coordinates according to the 'pointy' version at
    -- https://www.redblobgames.com/grids/hexagons/#coordinates-axial
    ( Int, Int )


coordToString : Coord -> String
coordToString ( q, r ) =
    "(" ++ String.fromInt q ++ "," ++ String.fromInt r ++ ")"


adjacents : Coord -> List Coord
adjacents ( q, r ) =
    [ ( q + 1, r )
    , ( q + 1, r - 1 )
    , ( q, r - 1 )
    , ( q - 1, r )
    , ( q - 1, r + 1 )
    , ( q, r + 1 )
    ]


rotateUnit : Coord -> Coord
rotateUnit ( q, r ) =
    ( q + r, -q )


add : Coord -> Coord -> Coord
add ( aq, ar ) ( bq, br ) =
    ( aq + bq, ar + br )


sub : Coord -> Coord -> Coord
sub ( aq, ar ) ( bq, br ) =
    ( aq - bq, ar - br )


genLR : Coord -> Coord -> List Coord
genLR src dst =
    [ add src (rotateUnit (sub dst src))
    , add src (List.foldr (always rotateUnit) (sub dst src) (List.range 0 4))
    ]
