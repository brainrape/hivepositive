module Engine.Bug exposing (..)


type Color
    = Black
    | White


colorToString : Color -> String
colorToString c =
    case c of
        Black ->
            "b"

        White ->
            "w"


type Species
    = QueenBee
    | Grasshopper
    | Spider
    | Ant
    | Beetle
    | Ladybug
    | Mosquito
    | Pillbug


speciesToString : Species -> String
speciesToString s =
    case s of
        QueenBee ->
            "Q"

        Grasshopper ->
            "G"

        Spider ->
            "S"

        Ant ->
            "A"

        Beetle ->
            "B"

        Ladybug ->
            "L"

        Mosquito ->
            "M"

        Pillbug ->
            "P"


type alias Bug =
    ( Color, Species, Int )


bugToString : Bug -> String
bugToString ( color, species, number ) =
    colorToString color ++ speciesToString species ++ String.fromInt number
