module Engine.Game exposing (..)

import Dict exposing (..)
import Engine.Action exposing (..)
import Engine.Bug exposing (..)
import Engine.Coord exposing (..)
import Engine.Field exposing (..)
import Engine.Movement exposing (..)
import List.Extra exposing (..)


type GameOver
    = Win Color
    | Draw


type alias Variant =
    List ( Species, Int )


basicVariant : Variant
basicVariant =
    [ ( QueenBee, 1 ), ( Beetle, 2 ), ( Grasshopper, 3 ), ( Spider, 2 ), ( Ant, 3 ) ]


fullVariant : Variant
fullVariant =
    [ ( QueenBee, 1 ), ( Beetle, 2 ), ( Grasshopper, 3 ), ( Spider, 2 ), ( Ant, 3 ), ( Pillbug, 1 ), ( Ladybug, 1 ), ( Mosquito, 1 ) ]


type alias Reserve =
    ( List (List Bug), List (List Bug) )


variantReserve : Variant -> Reserve
variantReserve specs =
    specs
        |> List.map (\( s, n ) -> List.range 1 n |> List.map (\i -> ( White, s, i )) |> List.reverse)
        |> (\xs -> ( xs, xs |> List.map (List.map (\( _, s, i ) -> ( Black, s, i ))) ))


reserve : Color -> Game -> List (List Bug)
reserve color game =
    let
        side =
            if color == White then
                Tuple.first

            else
                Tuple.second

        bugsPlayed =
            game.actions |> List.filterMap identity |> List.map Tuple.first
    in
    game.variant |> variantReserve |> side |> List.map (List.filter (\bug -> List.Extra.notMember bug bugsPlayed))


type alias Game =
    { variant : Variant
    , actions : List Action
    }


gameToString : Game -> String
gameToString game =
    String.join "; " (game.actions |> List.map actionToString)


getField : Game -> Field
getField game =
    game.actions
        |> List.filterMap identity
        |> List.foldl (\( b, c ) f -> removeBug b f |> addBug c b) Dict.empty


whoseTurn : List Action -> Color
whoseTurn xs =
    if modBy 2 (List.length xs) == 0 then
        White

    else
        Black


checkWin : Field -> Maybe GameOver
checkWin field =
    case ( getCoord ( White, QueenBee, 1 ) field, getCoord ( Black, QueenBee, 1 ) field ) of
        ( Just w, Just b ) ->
            case ( isSurrounded w field, isSurrounded b field ) of
                ( True, True ) ->
                    Just Draw

                ( True, False ) ->
                    Just (Win Black)

                ( False, True ) ->
                    Just (Win White)

                ( False, False ) ->
                    Nothing

        _ ->
            Nothing


genDestinations : Bug -> Game -> List Coord
genDestinations bug game =
    let
        field =
            getField game

        playerColor =
            whoseTurn game.actions

        ( bugColor, species, _ ) =
            bug

        restingBug =
            game.actions |> List.reverse |> List.head |> Maybe.andThen identity |> Maybe.map Tuple.first

        isPlaced =
            getCoord bug field /= Nothing

        isOwnBug =
            playerColor == bugColor

        isFourthMove =
            List.length game.actions == 6 || List.length game.actions == 7

        isQueenPlaced =
            field
                |> Dict.filter (\_ -> List.any (\( c, s, _ ) -> c == playerColor && s == QueenBee))
                |> (not << Dict.isEmpty)

        isOnTop =
            getCoord bug field |> Maybe.andThen (\c -> getTopBug c field) |> (==) (Just bug)
    in
    if
        False
            || (isPlaced && not isOnTop)
            || (Just bug == restingBug)
            || not (oneHive (removeBug bug field))
            || (checkWin field /= Nothing)
    then
        []

    else if not isQueenPlaced then
        if isOwnBug && not isPlaced && (not isFourthMove || species == QueenBee) then
            genPlaces bug (List.length game.actions) field

        else
            []

    else
        case getCoord bug field of
            Just src ->
                genThrows playerColor restingBug src field
                    ++ (if isOwnBug then
                            genMoves species src field

                        else
                            []
                       )

            Nothing ->
                if isOwnBug then
                    genPlaces bug (List.length game.actions) field

                else
                    []


checkAction : Action -> Game -> Bool
checkAction action game =
    case action of
        Just ( bug, coord ) ->
            genDestinations bug game |> List.filter ((==) coord) |> (not << List.isEmpty)

        Nothing ->
            game.variant
                |> variantReserve
                |> (\( w, b ) -> w ++ b)
                |> List.concat
                |> List.all (\bug -> genDestinations bug game |> List.isEmpty)


update : Action -> Game -> Maybe Game
update action game =
    if checkAction action game then
        Just { game | actions = game.actions ++ [ action ] }

    else
        Nothing
