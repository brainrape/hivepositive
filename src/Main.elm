module Main exposing (main)

import App exposing (..)
import Browser
import View exposing (..)


main : Program () Model Msg
main =
    Browser.sandbox
        { init = init
        , view = view
        , update = App.update
        }
