module App exposing (Model, Msg(..), init, update)

import Dict
import Engine.Bug exposing (Bug)
import Engine.Coord exposing (Coord)
import Engine.Field exposing (Field)
import Engine.Game exposing (Game, fullVariant, genDestinations, getField)


type Msg
    = Select (Maybe Bug)
    | Move Coord
    | Pass


type alias Model =
    { field : Field
    , selection : Maybe Bug
    , game : Game
    }


init : Model
init =
    { field = Dict.empty
    , selection = Nothing
    , game = { variant = fullVariant, actions = [] }
    }


update : Msg -> Model -> Model
update msg model =
    case msg of
        Select selection ->
            if selection == model.selection then
                { model | selection = Nothing }

            else
                case selection of
                    Just bug ->
                        if List.length (genDestinations bug model.game) > 0 then
                            { model | selection = selection }

                        else
                            model

                    Nothing ->
                        model

        Move coord ->
            model.selection
                |> Maybe.andThen
                    (\bug ->
                        Engine.Game.update (Just ( bug, coord )) model.game
                            |> Maybe.map
                                (\game ->
                                    { model
                                        | selection = Nothing
                                        , field = getField game
                                        , game = game
                                    }
                                )
                    )
                |> Maybe.withDefault model

        Pass ->
            Engine.Game.update Nothing model.game
                |> Maybe.map
                    (\game ->
                        { model
                            | selection = Nothing
                            , field = getField game
                            , game = game
                        }
                    )
                |> Maybe.withDefault model
